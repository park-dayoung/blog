class ArticlesController < ApplicationController
  http_basic_authenticate_with name: 'welcome', password: 'secret',
  except: [:index, :show]
  before_action :find_article, except: [:new, :create, :index]

  def index
  	@articles = Article.all
  end

  def new
  	@article = Article.new
  end

  def create
  	@article = Article.create(allowed_params)

  	if @article.valid?
  		redirect_to articles_path
  	else
  		render :new
  	end
  end

  def edit
  end

  def update
  	@article.update_attributes(allowed_params)

  	redirect_to articles_path
  end

  def show
  end

  def destroy
  	@article.destroy

  	redirect_to articles_path, notice: 'Delete success'
  end

  def allowed_params
  	params.require(:article).permit(:title, :description)
  end

  def find_article
  	@article = Article.find(params[:id])
  end
end
