class CommentsController < ApplicationController
	def create
		@article = Article.find(params[:article_id])
		allowed_params = params[:comment].permit(:commenter, :description)
		@comment = @article.comments.create(allowed_params)

		redirect_to @article
	end

	def destroy
		@article = Article.find(params[:article_id])
		@comment = @article.comments.find(params[:id])
		@comment.destroy

		redirect_to @article
	end
end
